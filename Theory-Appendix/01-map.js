//******************************************************************************** */
//***********This exercises are meant to be run with node on the console********** */
//******************************************************************************** */

const myArray = [1,2,3,1];

//map() let us create new arrays based on our existing arrays.

// Exercise: Return every element of the array plus one
console.log(myArray.map(element => element + 1));

//we will put "b" on the places where the function was called (the index of teh array)
myArray.map(() => "b");

//myArray still has the same value
console.log(myArray);