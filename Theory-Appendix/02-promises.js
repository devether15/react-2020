//******************************************************************************** */
//***********This exercises are meant to be run with node on the console********** */
//******************************************************************************** */


const myPromise = new Promise ((resolve, reject) =>{
    if(false){
        setTimeout(()=>{
          resolve('I have succeeded');  
        }, 1000)
    }else{
        reject('I have failed')
    }

})

myPromise
    .then(value => console.log(value))
    .catch(rejectValue => console.log(rejectValue))

//another example but with fetch

fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => console.log(json))
    .catch(error => console.log('I have errored'))