# React 2020

_Notes and exercices "hands on" from Gilbert Arévalo_ 

## Udemy 🚀

* **Course**: Complete React Developer in 2020 (w/ Redux, Hooks, GraphQL)
* **Instructors**: Andrei Neagoie / Yihua Zhang
* **available in**: https://www.udemy.com/course/complete-react-developer-zero-to-mastery/learn/lecture/15081792?start=75#overview

*"A user interface is like a joke. If you have to explain it, it’s not that good. "*
